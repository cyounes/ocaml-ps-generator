(*
 * Projet PostScript
 * 2012/2013
 * Etudiant: Cheikh Younes
 *)


(******* 
 * Exercice 6: 
 * * See file: exercice6.ps
 *******)


(******************** Auxiliary Functions *******************)

let write_to_file str outfile =
  let outf = open_out outfile in
    output_string outf str ; flush outf 
;;


(*
 * function return the negative number of a given number
 *)
let neg_ps_fun = fun() -> 
  "/neg { -1 mul } def\n" 
;;
(* val neg_ps_fun : unit -> string = <fun> *)

(*
 * Function return string of ps generated code to draw a circle 
 *)
let put_circle_def = fun() -> 
  "\n/circle {\n"^ 
  "\tnewpath\n"^
  "\t/$r exch def\n"^
  "\t/$y exch def\n"^
  "\t/$x exch def\n"^
  "\t$x $y $r 0 360 arc \n\tclosepath stroke\n} def\n\n"
;;

(*
let s = put_circle_def() in print_string s ;;
*)

(**
 * Function return string of ps generated code to draw a rectangle 
 *)
let put_rect_def = fun() ->
  "\n/rect {\n"^
  "\tnewpath\n"^
  "\t/$h exch def\n"^
  "\t/$w exch def\n"^
  "\t/$y exch def\n"^
  "\t/$x exch def\n"^
  "\t$x $y moveto\n"^
  "\t0 $h rlineto\n"^
  "\t$w 0 rlineto\n"^
  "\t0 $h neg rlineto\n"^
  "\t$w neg 0 rlineto\n"^
  "\tclosepath stroke\n} def\n\n"
;;

(*
let r = put_rect_def() in print_string r ;;
*)

(* postscript file header *) 
let header_ps = fun() -> 
  "%!PS-Adobe-2.0\n"^
  "%This is auto-generated file by ocaml\n0.1 setlinewidth\n\n"
;;

(* return concatination of previous returned strings *)
let prepared_str = fun() ->
  header_ps()^neg_ps_fun()^put_circle_def()^put_rect_def() 
;;

(* generate showpage keyword at the end of ps file *)
let endof_ps_file = fun () -> 
  "\n\n\nshowpage\n"
;;

(* function put header(functions ) + code to generate + showpage on ps file *)
let generate_output_file = fun 
  str outfile -> 
    let code = (prepared_str()^str^endof_ps_file()) in write_to_file code outfile
;;
(* val generate_output_file : string -> string -> unit = <fun> *)


(**************************** Exercices **********************************)

(*
 * Types Definitions 
 *)
type varname = 
  string 
;;

type expr =
  Const of float
| Var of varname
| Add of expr * expr
| Sub of expr * expr
| Mul of expr * expr 
;;


(*****************************)
(********* Exercice 7 ********)
(*****************************)

(*
 * Function return string of generated code to a given expression
 *)

let rec print_expr = function 
  Const(f) -> string_of_float(f)^" "
  |Var(str) -> str^" "
  |Add(e1, e2) -> print_expr(e1)^print_expr(e2)^"add "
  |Sub(e1, e2) -> print_expr(e1)^print_expr(e2)^"sub "
  |Mul(e1, e2) -> print_expr(e1)^print_expr(e2)^"mul "
;;
(* val print_expr : expr -> string = <fun> *)

(* TEST 
let s = print_expr(Add(Add(Const(1.),Var("x")), Const(5.))) ;;
print_string s ;;
*)


type coord = 
  Coord of expr * expr 
;;

(*
 * return string of coordinate x y type 
 *)
let print_coord = function
  Coord(e1,e2) -> print_expr(e1)^print_expr(e2) 
;;
(* val print_coord : coord -> string = <fun> *)



type shape =
  SCircle of expr
| SRectangle of expr * expr
;;

(*
 * Function print specified shape
 *)

let print_shape = function 
  SCircle(e) -> print_expr(e)^"circle\n" 
  |SRectangle(e1,e2) -> print_expr(e1)^print_expr(e2)^"rect\n" 
;;
(*val print_shape : shape -> string = <fun> *)


type stmt =
  Basic of coord * shape
| Assign of varname * expr
| Seq of stmt list
| For of varname * expr * expr * expr * stmt 
;;

(*
 * Exercice 8 : la reponse est dans le fichier de reponses
 * Exercice 9 : see file exercice9.ps
 * Exercice 10: see file exercice10.ps
 *)


    (***************************)
    (********* Exercice 11 *****) 
    (***************************)

let rec print_stmt = function 
  Basic(c, s) -> print_coord(c)^print_shape(s) 
  |Assign(v, e) -> "/"^v^" "^print_expr(e)^" def\n"  
  |Seq(s) -> if(List.length s) = 0 then "" 
              else if (List.length s) = 1 then print_stmt(List.hd s) 
              else print_stmt(List.hd s)^print_stmt(Seq(List.tl s))   
  |For(i, e1, e2, e3, s) -> print_expr(e1)^print_expr(e2)^print_expr(e3)^"{\n"^ 	(* start for loop *)
	"/"^i^" exch def\n"^ 			
	print_stmt(s)^"} for\n"		
;; 
(* val print_stmt : stmt -> string = <fun> *)

(* 
let circle = 
  put_circle_def() in print_string circle
;;
*)


    (***************************)
    (********* Exercice 12 *****) 
    (***************************)
let s = For ("i", Const 0.0, Const 20.0, Const 120.0,
  Basic (Coord(Add(Const 100.0, Var "i"), Const 200.0),
    SCircle(Add((Mul(Const 1.2, Var "i")), Const 20.0))))
in let ss = print_stmt s
in generate_output_file ss "../ps/exercices/exercice12.ps" 
;;


type decl = 
  Decl of varname * expr 
;; 

type prog = 
  Prog of decl list * stmt 
;;

      (******************************)
      (******* Exercice 14 **********)
      (******************************)

let print_decl (Decl(v,e)) =  
  "/"^v^" "^print_expr(e)^"def\n" 
;;
(* val print_decl : decl -> string = <fun> *)

 (*
 * function return string
 * concatenating a list of strings 
 *)
let rec print_string_list = function 
  [] -> ""
  | s::l -> s^print_string_list(l) 
;;
(* val print_string_list : string list -> string = <fun> *) 

let print_prog = function 
   Prog(dl, s) -> (print_string_list(List.map print_decl dl))^print_stmt(s) ;; 

(* val print_prog : prog -> string = <fun> *)

type rw_status = 
  R | RW 
;; 

(**
 * function: vlist_contains
 * param: varname v & list(varname, rw_status) 
 * This function return int :
   * false if the variable v doesn't exist
 *)

let rec vlist_contains = function
  (_, []) -> false (* return false, and don't raise excpetion 
                      The exception will be raised next on the function that
                      invoke this function with a personalized message *)
  | ( (a:varname) , (( v:varname)  ,permission)::l) -> if a = v then true else vlist_contains(a,l)
;;

(*
 * val vlist_contains : varname * (varname * rw_status) list -> int = <fun> 
 *)

        (******************************)
        (*********** Exercice 15 ******)
        (******************************)

let rec tc_expr = function
    (vars, Const(_))    -> true
  | (vars, Var(v) )    -> vlist_contains(v, vars) 
  | (vars, Add(e1,e2)) -> tc_expr(vars, e1) && tc_expr(vars, e2) 
  | (vars, Sub(e1,e2)) -> tc_expr(vars, e1) && tc_expr(vars, e2) 
  | (vars, Mul(e1,e2)) -> tc_expr(vars, e1) && tc_expr(vars, e2)
;;
(*
 * val tc_expr : (varname * rw_status) list * expr -> bool = <fun> 
 *)


        (******************************)
        (*********** Exercice 16 ******)
        (******************************)

let tc_coord = function 
  vars, Coord(e1, e2) ->  tc_expr(vars , e1) && tc_expr(vars, e2) 
;;
(* 
 * val tc_coord : (varname * rw_status) list * coord -> bool = <fun> 
 *)

let tc_shape = function
  (vars, SCircle (e)) -> tc_expr(vars, e) 
  | (vars, SRectangle(e1,e2)) -> tc_expr(vars, e1) && tc_expr(vars, e2) 
;;
(*
 * val tc_shape : (varname * rw_status) list * shape -> bool = <fun>
 *)



        (******************************)
        (*********** Exercice 17 ******)
        (******************************)
let rec tc_stmt = function 
  (vars, Basic(c,s)) -> 
    tc_coord(vars, c) && tc_shape(vars, s) 
  | (vars, Assign(v, e) ) -> 
    if vlist_contains(v, vars) then 
      tc_expr(vars, e) 
    else
      failwith("Error: Need RW permission"^v)
  | (vars, Seq(s) ) -> 
    if (List.length s) = 1 then
      tc_stmt(vars, List.hd s) 
    else
      tc_stmt(vars, (List.hd s)) && tc_stmt(vars, (Seq(List.tl s))) 
  | (vars, For(v,e1,e2,e3,s)) -> 
    if vlist_contains(v, vars) then
      failwith("Error: Double declaration\n"^v^" was here!)") 
    else
      tc_stmt(vars@[v,R], s)
;;

(*
 * val tc_stmt : (varname * rw_status) list * stmt -> bool = <fun>
 *)


        (******************************)
        (*********** Exercice 18 ******)
        (******************************)

let rec tc_decls = function
  (vars, [] ) -> vars
  |(vars, Decl(v,e)::ds) -> 
    if vlist_contains(v, vars) then
      failwith("Error: Double declaration\n"^v^" was here!")
    else if not(tc_expr(vars, e)) then
      failwith("Type Error in: \n"^print_expr(e)) 
    else
      let newVlist = vars@[v,RW] in 
      tc_decls(newVlist, ds) 
;;

(*
 * val tc_decls : (varname * rw_status) list * decl list -> (varname * rw_status) list = <fun>
 *)



        (******************************)
        (*********** Exercice 19 ******)
        (******************************)

let tc_prog = function 
  Prog(ds, s) -> 
    let vars = tc_decls([], ds) in 
    tc_stmt(vars, s) 
;;
(*
 * val tc_prog : prog -> bool = <fun>
 *)




      (*****************************************************)
      (*****************************************************)
      (*********************** T E S T S *******************)
      (*****************************************************)
      (*****************************************************)

let p1 = Prog([Decl("init", Const 0.0);
      Decl("step", Const 10.0);
      Decl("stop", Mul(Const 5.0, Var "step"));
      ],
      (For ("i", Var "init", Var "step", Var "stop",
        For ("j", Const 0.0, Const 20.0, Const 60.0,
          Basic (Coord(Add(Var "i", Const 100.0), Add(Var "j", Const 400.0)),
            SRectangle(Const 3., Const 5.))
            )
        )
    )
)
;;

(* Generate code *)
if (tc_prog p1) then generate_output_file (print_prog(p1) ) "../ps/tests/p1.ps" ;;
(*
 * # tc_prog p1 ;;
 * - : bool = true
 *)



let p2 = Prog([Decl("init", Const 0.0);
      Decl("stop", Mul(Const 5.0, Var "step"));
      Decl("step", Const 10.0);
      ],
      (For ("i", Var "init", Var "step", Var "stop",
        For ("j", Const 0.0, Const 20.0, Const 60.0,
          Basic (Coord(Add(Var "i", Const 100.0), Add(Var "j", Const 400.0)),
            SRectangle(Const 3., Const 5.))
            )
        )
    )
)
;;
(*
 * # tc_prog p2 ;; 
 * Exception: Failure "Variable undefined: step".
 *)

let p3=Prog([Decl("x", Const 10.);
      Decl("y", Const 20.);
      ],
      (For ("i", Const 0., Const 20., Const 100.,
        Seq [
          Assign("x", Add(Var "x", Var "i"));
          Assign("y", Add(Var "y", Var "i"));
          Basic (Coord(Var "x", Var "y"), SRectangle(Const 3., Const 5.))
            ]
      )
  )
)
;;
if (tc_prog p3) then generate_output_file (print_prog(p3) ) "../ps/tests/p3.ps" ;;
(*
 * # tc_prog p3 ;;
 * - : bool = true
 *)
